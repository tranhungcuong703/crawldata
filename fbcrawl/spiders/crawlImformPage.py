 # -*- coding: utf-8 -*-
import scrapy
from scrapy_splash import SplashRequest


class CrawlImformation(scrapy.Item):
    namePage = scrapy.Field()
    likedPage = scrapy.Field()
    followedPage= scrapy.Field()
    dateCreatePage = scrapy.Field()

class FacebookSpider(scrapy.Spider):

    name = "imformPage"
    allowed_domains = ['facebook.com']
    start_urls = ["https://www.facebook.com/hustconfession/"]
    custom_settings = {
        'FEED_EXPORT_FIELDS': ['namePage','dateCreatePage','likedPage','followedPage' ]
    }
    def start_requests(self):
        url=self.page
        yield SplashRequest(url, endpoint="render.html", callback=self.parse)

    def parse(self, response):
        item = CrawlImformation()
        item["namePage"]= response.css("a._64-f span::text").extract_first()
        item["likedPage"] = response.css("div._4bl9 div::text").extract_first()
        item["followedPage"] =  response.css("div._2pi2 div._4bl9 div::text")[1].extract()
        item["dateCreatePage"] = response.css("div._3qn7._61-0._2fyi._3qnf._2pi9._3-95 span::text").extract_first()
        yield item


