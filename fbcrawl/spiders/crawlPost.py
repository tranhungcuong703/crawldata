# -*- coding: utf-8 -*-
import scrapy
import logging
from scrapy_splash import SplashRequest

lua_script = """
    function main(splash)
            local num_scrolls = 15
        local scroll_delay = 1.0
        local scroll_to = splash:jsfunc("window.scrollTo")
        local get_body_height = splash:jsfunc(
            "function() {return document.body.scrollHeight;}"
        )
        assert(splash:go(splash.args.url))
        splash:wait(splash.args.wait)
        for _ = 1, num_scrolls do
            scroll_to(0, get_body_height())
            splash:wait(scroll_delay)
        end
        return splash:html()
    end
    """


class CrawlPost(scrapy.Item):
    date = scrapy.Field()
    like = scrapy.Field()
    haha = scrapy.Field()
    love = scrapy.Field()
    wow = scrapy.Field()
    sad = scrapy.Field()
    angry= scrapy.Field()
    contentPost= scrapy.Field()
    reactions = scrapy.Field()
    comments = scrapy.Field()
    shares =scrapy.Field()
    url = scrapy.Field()    

class FacebookSpider(scrapy.Spider):

    name = "postPage"
    allowed_domains = ['facebook.com']
    
    custom_settings = {
        'FEED_EXPORT_FIELDS': ['contentPost','date', \
                                'reactions','like','love','haha','wow', \
                               'sad','angry','comments','shares','url']
    }

    def start_requests(self):
        url= self.page
        yield SplashRequest(url=url, callback=self.parse,
                                endpoint="execute",
                                args={'wait': 2, 'lua_source': lua_script},)

    def parse(self, response):
        number=self.counts
        index = 0
        if(number=='all'):
            numbers=10000
        else:
            numbers=int(number)
        item = CrawlPost()
        for data in response.css("div._427x "):
            item["date"] = data.css("a._5pcq abbr::attr(title)").extract_first()
            item["like"] = data.css("div._66lg span[data-testid='UFI2TopReactions/tooltip_LIKE'] a::attr(aria-label)").extract_first()
            item["love"] = data.css("div._66lg span[data-testid='UFI2TopReactions/tooltip_Love'] a::attr(aria-label)").extract_first()
            item["haha"] = data.css("div._66lg span[data-testid='UFI2TopReactions/tooltip_HAHA'] a::attr(aria-label)").extract_first()
            item["wow"] = data.css("div._66lg span[data-testid='UFI2TopReactions/tooltip_WOW'] a::attr(aria-label)").extract_first()
            item["sad"] = data.css("div._66lg span[data-testid='UFI2TopReactions/tooltip_SORRY'] a::attr(aria-label)").extract_first()
            item["angry"] = data.css("div._66lg span[data-testid='UFI2TopReactions/tooltip_ANGER'] a::attr(aria-label)").extract_first()
            item["contentPost"] = data.css("div._3576 p::text").extract()
            item["reactions"] = data.css("div._66lg span._3dlg span::text").extract_first()
            item["comments"] = data.css("div._4vn1 a._3hg-::text").extract_first()
            item["shares"] = data.css("div._4vn1 a._3rwx::text").extract_first()
            item["url"] = data.css("a._5pcq::attr(href)").extract_first()
            if len(item["contentPost"]) == 0 :
            	item["contentPost"]=data.css("span._4a6n::text").extract_first()
            	if item["contentPost"] is None:
            		item["contentPost"] = item["url"]
            index +=1
            if(index>numbers):
                break
            yield item

